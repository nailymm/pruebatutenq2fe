# Question2FE

Este proyecto fue generado en [Angular CLI](https://github.com/angular/angular-cli) version 12.1.1.

## Especificaciones tecnicas
Angular 12.1.1

## Setup:
Clonar repositorio.
`$ git clone git@gitlab.com:nailymm/pruebatutenq2fe.git`
`$ cd bluesoft-test`

Hacer el install con npm.
`$ npm install`

Ejecutar con 
`$ ng serve`

La aplicacion queda disponible en: http://localhost:4200

## Mejoras futuras:
- Aplicacion de Angular Material o cualquier plantilla customizable.

