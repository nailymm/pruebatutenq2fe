import { Component } from '@angular/core';
import { TimeService } from './services/time.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'question2FE';
  time = '';
  zone = '';
  zones : any;
  selectedValue: any;
  timeConverted: any;

  constructor (private timeService: TimeService) {}
  ngOnInit(): void {
    this.timeService.getAllTimeZones().subscribe(response => {
      console.log(response);
      this.zones = response;
    },
    error => {
      console.log(error);
    });
  }
  
  convertTime(): void {
    console.log(this.time);
    this.zone = this.selectedValue;
    console.log('zone', this.zone);
    this.timeService.converTime$(this.time, this.zone).subscribe(response => {
      console.log(response);
      this.timeConverted = JSON.stringify(response);
    },
    error => {
      console.log(error);
    });
  }
}
