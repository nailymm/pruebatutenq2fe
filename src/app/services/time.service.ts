import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const baseURL = 'http://localhost:8080/api';

@Injectable({
  providedIn: 'root'
})
export class TimeService {
  
  constructor(private httpClient: HttpClient) { }

  converTime$(time: string, zone: string): Observable<any> {
    let body = new HttpParams()
    .set('time', time).set('zone', zone);
    return this.httpClient.post(baseURL+'/convertTime', body);
  }

  getAllTimeZones(): Observable<any>{
    return this.httpClient.get(baseURL+'/allTimeZones');
  }
}
